var map = L.map('main_map', {
    center: [51.53, -0.07],
    zoom: 13
});


L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

//L.marker([51.505, -0.06]).addTo(map);
//L.marker([51.52, -0.07]).addTo(map);
//L.marker([51.53, -0.09]).addTo(map);
//L.marker([51.54, -0.08]).addTo(map);

//request asincrónico para hacer una solicitud a una web en formato json
$.ajax({
    dataType: "json", //tipo de dato
    url: "api/bicicletas", //web donde se va a poner
    success: function(result){  //resultado de la operación
        console.log(result);
        result.Bicicleta.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})
