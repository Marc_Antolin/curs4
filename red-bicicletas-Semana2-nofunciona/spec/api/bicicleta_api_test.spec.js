var Bicicleta = require('../../models/bicicleta');
var request = require('request');
// importamos la variable del servidor 
var server = require('../../bin/www')

var base_url = 'http://localhost:3000/api/bicicletas';
//Logea en la consola para indicar que se está ejecutando el test
beforeEach(() =>{Bicicleta.allBicis = [];console.log("testeando"); });

describe('Bicicleta API', ()=>{
    describe('get bicicletas', ()=>{
        it('Status 200', ()=>{
            //comprueba que la lista está vacía
            expect(Bicicleta.allBicis.length).toBe(0);
            //prueba añadir un elemento a la lista
             var a = new Bicicleta(1, 'negro', 'urbana', [-34.5712424, -58.3861497]);
            Bicicleta.add(a);
            //Ejecutamos un get para simul
            request.get(base_url, function (error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });
    //Simulamos un envío de información (POST)
    describe('post bicicletas /create', ()=>{
        it('Status 200', (done)=>{
            //enviamos cabeceras
            var headers = {'content-type': 'application/json'};
            //enviamos un string de prueba
            var aBici = '{"id": 10, "color": "negro", "modelo": "urbana", "lat": -34, "lng": -58}';
            //simulamos una creación de registro (Create)
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("negro");
                done(); // "Done" es la ord en que tiene Jasmine para indicar que se ha acabado el test
            });
       });
    });
    describe('delete bicicletas /delete', ()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id": 10, "color": "negro", "modelo": "urbana", "lat": -34, "lng": -58}';
            
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("negro");
                request.delete({
                    headers: headers,
                    url: base_url + '/delete',
                    body: '{"id": 10}'
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(204);
                    done();
                });
            });
        });
    });
});
