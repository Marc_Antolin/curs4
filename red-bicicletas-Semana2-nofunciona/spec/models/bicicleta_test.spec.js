//Declarar modelos que vamos a utilizar MOngoDB y los de bicicleta
var mongoose = require('mongoose'); 
var  Bicicleta = require ('../../models/bicicleta');

describe('Test Bicicletas', function(){
    var originalTimeout;
 //Antes de cada test nos conectamos y registramos en consola
    beforeEach(function(done) { 
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        console.log(jasmine.DEFAULT_TIMEOUT_INTERVAL);
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function() {
            console.log('coneccion establecida');
            done();
        });
    });
//Después de cada test, borramos todo lo introducido
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });
//Prueba de crear una instancia un elemento
    describe('Bicicleta.createInstance', () =>{
        it('crea una instancia de bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, 'Morado', 'urbana', [-34.5, -58.3]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Morado');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-58.3);
        });
    });
// Probamos que muestre todo. Primero comprueba que aparezca vacía
    describe('Bicicleta.allBicis', () =>{
        it('comienza vacia', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();                
            });
        });        
    });
//Probvamos que añada una bicicleta
    describe('Bicicleta.add', () =>{
        it('agrega solo una bicicleta', (done) =>{
            var aBici = new Bicicleta({code:1, color: 'verde', modelo:"urbana"});
            Bicicleta.add(aBici,function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();                
                });
            });            
        });        
    });
// Prueba de búsqueda de bici. Crea una y  la busca
    describe('Bicicleta.findByCode', () =>{
        it('agrega solo una bicicleta', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0); //prueba si está vacía y luego añade una
                var aBici = new Bicicleta({code:1, color: 'verde', modelo:"urbana"});
                Bicicleta.add(aBici,function(err, newBici){
                    if(err) console.log(err);
//añade otra bici para probar
                    var aBici2 = new Bicicleta({code:2, color: 'roja', modelo:"urbana"});
                    Bicicleta.add(aBici2,function(err, newBici){
                        if(err) console.log(err);
//busca la primera bici creada
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();                
                        });
                    });
                });  
            });                          
        });        
    });
});

/* Pruebas previas a la  integración con MongoDB
//método para ejecutar un código antes de cada test
beforeEach(() => { Bicicleta.allBicis = []; });

describe ('Bicicleta.allBicis', () => {
    it ('comienza vacía', () => {
        expect (Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => { 
    it ('Comprobar si añade una bici', () => {
    //precondición, que la lista esté vacía
    expect (Bicicleta.allBicis.length).toBe(0);
    //añadimos una de prueba
    var a = new Bicicleta(1, 'rojo', 'urbana', [51.505, -0.06]);
    Bicicleta.add(a);
    //testeamos que la lista tenga esta bicicleta añadida
    expect (Bicicleta.allBicis.length).toBe(1);
    //comprobamos que la bicicleta añadida sea "a"
    expect (Bicicleta.allBicis[0]).toBe(a);
    }); 
});

describe ('Bicicleta.findById',() => {
    it ('Debe devolver la bici con id 1',() =>{

  //precondición, que la lista esté vacía
  expect (Bicicleta.allBicis.length).toBe(0);
  //añade dos bicis
  var aBici1 = new Bicicleta(1,"verde","urbana");
  var aBici2 = new Bicicleta(2,"roja","montaña");
  Bicicleta.add(aBici1);
  Bicicleta.add(aBici2);

  var targetBici = Bicicleta.findById(1);
  expect (targetBici.id).toBe(1);
  expect (targetBici.color).toBe(aBici1.color);
  expect (targetBici.modelo).toBe(aBici1.modelo);
    });
});*/