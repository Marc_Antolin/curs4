//importamos el modelo de bicicleta (Models/bicicleta.js)
var Bicicleta = require('../models/bicicleta');

//Modelo para mostrar todas las bicicletas
exports.bicicleta_list = function (req,res) {
    res.render('bicicleta/index', {bicis: Bicicleta.allBicis}); //vista tabla bicicletas (Views\bicicleta\index.pug)
}
// Modelo primer momento que es mostrar el formulario de creación de bicicleta
exports.bicicleta_create_get = function (req,res) {
    res.render('bicicleta/create'); 
}
// Modelo para la creación  de la bicicleta
exports.bicicleta_create_post = function (req,res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas'); // una vez entrada la bicicleta, te redirije a la pagina donde se listan
// Ahora en la carpeta routers
}


//una vez añadido el módulo, se añade la ruta (routes/bicicleta.js)

exports.bicicleta_update_get = function (req,res) {
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicleta/update', {bici});
}

// modulo para actualizar los datos de la bicicleta
exports.bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');
}
//modulo de eliminación de bicicleta (Models\bicicleta.js)
exports.bicicleta_delete_post = function (req,res) {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}