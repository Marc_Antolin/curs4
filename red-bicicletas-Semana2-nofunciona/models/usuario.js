var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
//Crea el esquema del usuario 
var usuarioSchema = new Schema({
    nombre: String
});
//pasamos el ide de la bicicleta  a
usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta}); //crea la reserva
    console.log(reserva); //logea en la consola 
    reserva.save(cb); //guarda la reserva
}

module.exports = mongoose.model('Usuario', usuarioSchema);