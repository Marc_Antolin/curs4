var express = require('express')
var router = express.Router();
var BicicletaController = require('../../controllers/api/bicicletaControllerAPI')
//añadidos desde controlers/bicicletas.js)
router.get('/bicicletas', BicicletaController.bicicleta_list);
//router.post('/create', BicicletaController.bicicleta_create_get);
//router.post('/create', BicicletaController.bicicleta_create_post);
//router.post('/create', BicicletaController.bicicleta_delete_post);
// Una vez añadidos las routas, modificamos la vista en bicibletas\index.pug para crear un link al borrado

router.post('/create', BicicletaController.bicicleta_create);
router.post('/delete', BicicletaController.bicicleta_remove);

module.exports = router;