var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + " ~ color: " + this.color; 
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}
// método para hacer una búsqueda  con el método .find
Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) {
        return aBici;
    } else {
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);  
    }
}
// método para eliminar . Recibe el parámetro y lo busca mediante una iteracion (For)
//
Bicicleta.removeById = function (aBiciId) {
    Bicicleta.findById(aBiciId);
    for (let index = 0; index < Bicicleta.allBicis.length; index++) {
        if (Bicicleta.allBicis[index].id == aBiciId) {
            Bicicleta.allBicis.splice(index,1); //splice elimina el elemento de la vista
            break;
        }        
    }
}
//una vez añadido el método hay que añadir el controler (controlers/bicicleta.js)

//Este código se comenta pq en la semana 2 se requiere que la lista empiece vacía
/*añadir dos elementos al array anterior de manera estática
var a = new Bicicleta(1, 'rojo', 'urbana', [51.505, -0.06]);
var b = new Bicicleta(2, 'blanca', 'urbana', [51.515, -0.09]);

//añadir elementos estáticos
Bicicleta.add(a);
Bicicleta.add(b); */

module.exports = Bicicleta;
