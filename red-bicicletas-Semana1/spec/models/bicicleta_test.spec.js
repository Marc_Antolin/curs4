var  Bicicleta = require ('../../models/bicicleta');

//método para ejecutar un código antes de cada test
beforeEach(() => { Bicicleta.allBicis = []; });

describe ('Bicicleta.allBicis', () => {
    it ('comienza vacía', () => {
        expect (Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => { 
    it ('Comprobar si añade una bici', () => {
    //precondición, que la lista esté vacía
    expect (Bicicleta.allBicis.length).toBe(0);
    //añadimos una de prueba
    var a = new Bicicleta(1, 'rojo', 'urbana', [51.505, -0.06]);
    Bicicleta.add(a);
    //testeamos que la lista tenga esta bicicleta añadida
    expect (Bicicleta.allBicis.length).toBe(1);
    //comprobamos que la bicicleta añadida sea "a"
    expect (Bicicleta.allBicis[0]).toBe(a);
    }); 
});

describe ('Bicicleta.findById',() => {
    it ('Debe devolver la bici con id 1',() =>{

  //precondición, que la lista esté vacía
  expect (Bicicleta.allBicis.length).toBe(0);
  //añade dos bicis
  var aBici1 = new Bicicleta(1,"verde","urbana");
  var aBici2 = new Bicicleta(2,"roja","montaña");
  Bicicleta.add(aBici1);
  Bicicleta.add(aBici2);

  var targetBici = Bicicleta.findById(1);
  expect (targetBici.id).toBe(1);
  expect (targetBici.color).toBe(aBici1.color);
  expect (targetBici.modelo).toBe(aBici1.modelo);
    });
});